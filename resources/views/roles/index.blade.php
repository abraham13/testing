@extends('layouts.app')
@section('content')


  @include('flash::message')
  <div class="col-lg-12">
    <div class="card-header">
        <i class="fas fa-table"></i>
        Roles
        <a  class="btn-sm" style="float:right;" href="{!! route('roles.create') !!}">Nuevo rol</a>
    </div>
        <div class="card-body">
            @include('roles.table')
        </div>
    </div>
 @endsection