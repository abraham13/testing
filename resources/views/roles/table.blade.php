 
     <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                    <th>Nombre</th>                  
                    <th colspan="3">Acciones
                           
                    </th>
                </tr>
              </thead>              
              <tfoot>
                <tr>
                    <th>Nombre</th>
                    
                    <th colspan="3">Acciones </th>
                </tr>
              </tfoot>
              <tbody>
                    @foreach ($roles as $key => $role)
            <tr>
                <td>{!! $role->name !!}</td>               
                
                <td>
                    {!! Form::open(['route' => ['roles.destroy', $role->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                    
                     @can('role-edit')
                        <a href="{!! route('roles.edit', [$role->id]) !!}" class='btn btn-info btn-sm'><i class="far fa-edit"></i></a>
                     @endcan
                     @can('role-delete')
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    @endcan
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
               
              </tbody>
            </table>
    </div>