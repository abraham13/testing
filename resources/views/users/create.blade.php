@extends('layouts.app')

@section('content')

    <div class="content col-lg-12">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="offset-4 row">
                    {!! Form::open(['route' => 'users.store', 'class'=>'col-xl-12']) !!}

                        @include('users.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
