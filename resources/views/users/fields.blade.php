
    <!-- Abreviatura Field -->
  
        <h1 >Usuario</h1>
        <div class="eventoVistas form-group col-sm-6">
            {!! Form::label('name', 'Nombre') !!}
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Nombre Field -->
        <div class="eventoVistas form-group col-sm-6">
            {!! Form::label('email','Email') !!}
            {!! Form::email('email', null, ['class' => 'form-control']) !!}
        </div>
        <!-- Nombre Field -->
        <div class="eventoVistas form-group col-sm-6">
            {!! Form::label('password','Contraseña') !!}
            {!! Form::password('password', null, ['class' => 'form-control']) !!}
        </div>
     
        
        <div class="eventoVistas form-group col-sm-12">
        {!! Form::label('roles', 'Roles') !!}        
            <ul class="list-unstyled">
                @foreach($roles as $role)
                <li>
                    <label>
                    {{ Form::checkbox('roles[]', $role->id, null) }}
                    {{ $role->name }}
                    </label>
                </li>
                @endforeach
            </ul>
        </div>

         <!-- Submit Field -->
         <div class="form-group col-sm-12" id="evento_botones">
            {!! Form::submit('Enviar', ['class' => 'btn btn-primary']) !!}
            <a href="{!! route('users.index') !!}" class="btn btn-default">Cancel</a>
            
        </div>
   



