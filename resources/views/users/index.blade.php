@extends('layouts.app')
@section('content')


  @include('flash::message')
  <div class="col-lg-12">
    <div class="card-header">
        <i class="fas fa-table"></i>
        Usuarios
        <a  class="btn-sm" style="float:right;" href="{!! route('users.create') !!}">Nuevo usuario</a>
    </div>
        <div class="card-body">
            @include('users.table')
        </div>
    </div>       

 @endsection