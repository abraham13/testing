@extends('layouts.app')

@section('content')
    
    <div class="content col-lg-12">
        <div class="box box-primary">
            <div class="box-body">
                    <div class="offset-4 row">
               

                        {!! Form::model($user, ['route' => ['users.update', $user->id], 'method' => 'patch']) !!}

                            @include('users.show_fields')
                            
                        {!! Form::close() !!}

                
                </div>
            </div>
        </div>
    </div>
@endsection

