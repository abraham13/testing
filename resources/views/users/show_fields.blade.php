
        <h1 class=" offset-1 col-sm-12" >Usuario</h1>
        <div class="eventoVistas form-group offset-1 col-sm-6">
            {!! Form::label('name', 'Nombre') !!}
            {!! Form::text('name', $user->name, ['class' => 'form-control']) !!}
        </div>

        <!-- Nombre Field -->
        <div class="eventoVistas form-group offset-1  col-sm-6">
            {!! Form::label('email','Email') !!}
            {!! Form::email('email',  $user->email, ['class' => 'form-control']) !!}
        </div>

        <!-- Grupo Field -->
        <div class="eventoVistas form-group offset-1 col-sm-6">
            {!! Form::label('password', 'Contraseña') !!}
            {!! Form::text('password', null, ['class' => 'form-control']) !!}
        </div>

           
        
        <div class="eventoVistas form-group offset-1 col-sm-6">
        {!! Form::label('roles', 'Asignar rol') !!}        
            <ul class="list-unstyled">
                @foreach($roles as $role)
                <li>
                    <label>
                    {{ Form::checkbox('roles[]', $role->id, null) }}
                    {{ $role->name }}
                    </label>
                </li>
                @endforeach
            </ul>
        </div>

        
        <div class="form-group offset-1 col-sm-12">
                <a href="{!! route('users.index') !!}" class="btn btn-default">Back</a>
           </div>
       

