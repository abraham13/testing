
     <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Email</th>
                    <th>Rol</th>
                    <th colspan="3">Acciones
                               
                    </th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                    <th>Nombre</th>
                    <th>Email</th>
                    <th>Rol</th>
                    <th colspan="3">Acciones </th>
                </tr>
              </tfoot>
              <tbody>
                    @foreach ($data as $key => $user)
                    <tr>
                        <td>{!! $user->name !!}</td>
                        <td>{!! $user->email !!}</td>
                        <td> @if(!empty($user->getRoleNames()))
                                @foreach($user->getRoleNames() as $v)
                                <label class="badge badge-success">{{ $v }}</label>
                                @endforeach
                            @endif
                        </td>
                        
                        <td>
                            {!! Form::open(['route' => ['users.destroy', $user->id], 'method' => 'delete']) !!}
                            <div class='btn-group'>
                                <a dusk="show{!!$user->id !!}" href="{!! route('users.show', [$user->id]) !!}" class='btn btn-success btn-sm'><i class="far fa-eye"></i></a>
                                <a dusk="edit{!!$user->id !!}"href="{!! route('users.edit', [$user->id]) !!}" class='btn btn-info btn-sm'><i class="far fa-edit"></i></i></a>
                                {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'onclick' => "return confirm('Are you sure?')"]) !!}
                            </div>
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
               
              </tbody>
            </table>
    </div>