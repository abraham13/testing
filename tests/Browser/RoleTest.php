<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\User;
use App\Models\Roles;
use DB;

class RoleTest extends DuskTestCase
{
    /**
     * testshowsRolesList by route.
     *
     * @return void
     */
  
    function testshowsRolesList()
    {     

        $this->browse(function ($browse) {
            $browse ->loginAs(User::find(1))
                    ->visit('/roles')
                    ->waitForText('Roles')
                    ->assertSee('Roles')                    
                    ->assertSee('admin');
        });       
    }
     /**
     * testshowsRolesList by interface.
     *
     * @return void
     */
    /*function testshowsRolesList2()
    {     

        $this->browse(function ($browse) {
            $browse ->loginAs(User::find(1))
                    ->visit('/home')              
                    ->clickLink('@MyProfile')
                    ->clickLink('@roles')
                    ->assertSee('Roles')
                    ->assertSee('operator')
                    ->assertSee('admin')
                    ->assertSee('user');
              
        });       
    }*/

    /**
     * testRolesCreate by route.
     *
     * @return void
     */
    function testRolesCreate()
    {     

        $this->browse(function ($browse) {
            $browse ->loginAs(User::find(1))
                    ->visit('/roles/create')
                    ->waitForText('Rol')
                    ->assertSee('Rol')
                    ->type('name', 'pruebas')                               
                    ->press('Enviar')
                    ->visit('/roles')
                    ->assertSee('pruebas');
                
        });  
            
    }
    /**
     * testshowsRolesDetails by route.
     *
     * @return void
     */
    function testshowsRolesDetails()
    {     
        $id=DB::table('roles')->where('name','pruebas')->where('deleted_at',null)->value('id');
        $this->browse(function ($browse) use ($id) {
            $browse ->loginAs(User::find(1))
                    ->visit('/roles/'.$id)
                    ->waitForText('Rol')
                    ->assertSee('Rol')
                    ->assertInputValue('name', 'pruebas');
                    
              
        });       
    }

    /**
     * testshowsRolesDetails by interface.
     *
     * @return void
     */
    function testshowsUsersDetails2()
    {     
        $id=DB::table('roles')->where('name','pruebas')->where('deleted_at',null)->value('id');
        $this->browse(function ($browse) use ($id) {
            $browse ->loginAs(User::find(1))
                    ->visit('/roles')
                    ->waitForText('Roles')
                    ->assertSee('Roles')
                    ->click('@show'.$id)
                    ->assertInputValue('name', 'pruebas');
              
        });       
    }

    /**
     * testRolEdit by route.
     *
     * @return void
     */
    function testRolEdit()
    {     
        $id=DB::table('roles')->where('name','pruebas')->where('deleted_at',null)->value('id');
        $this->browse(function ($browse) use ($id) {
            $browse ->loginAs(User::find(1))
                    ->visit('/roles/'.$id.'/edit')
                    ->waitForText('Rol')
                    ->assertSee('Rol')
                    ->assertInputValue('name', 'pruebas')         
                     ->type('name','Hola')
                    ->press('Enviar')
                    ->visit('/roles/'.$id.'/')
                    ->assertInputValue('name', 'Hola');
              
        });       
    }

     /**
     * testRolEdit2 by route.
     *
     * @return void
     */
    function testRolEdit2()
    {     
        $id=DB::table('roles')->where('name','Hola')->where('deleted_at',null)->value('id');
        $this->browse(function ($browse) use ($id) {
            $browse ->loginAs(User::find(1))
                    ->visit('/roles')
                    ->waitForText('Roles')
                    ->assertSee('Roles')
                    ->click('@edit'.$id)
                    ->assertInputValue('name', 'Hola')                   
                    ->type('name','pruebas')
                    ->press('Enviar')
                    ->visit('/roles/'.$id.'/')
                    ->assertInputValue('name', 'pruebas');
              
        });       
    }
    /**
     * testRolesDelete by interface.
     *
     * @return void
     */
    function testRolesDelete()
    {     

       /* $this->browse(function ($browse) {
            $browse ->loginAs(User::find(1))
                    ->visit('/roles')
                    ->assertVisible('#dataTable > tbody:nth-child(3) > tr:nth-child(1) > td:nth-child(2) > form:nth-child(1) > div:nth-child(3) > button:nth-child(2)')
                    ->visit($browse->press('#dataTable > tbody:nth-child(3) > tr:nth-child(1) > td:nth-child(2) > form:nth-child(1) > div:nth-child(3) > button:nth-child(2)', 'href')->acceptDialog()->assertSee('Usuarios'));
                    //->visit('/users')
                                 
       }); */      
    }

    
  
    
}
