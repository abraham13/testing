<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\User;
use DB;

class UserTest extends DuskTestCase
{
    /**
     * testshowsUsersList by route.
     *
     * @return void
     */
  
    function testshowsUsersList()
    {     

        $this->browse(function ($browse) {
            $browse ->loginAs(User::find(1))
                    ->visit('/users')
                    ->waitForText('Usuarios')
                    ->assertSee('admin')
                    ->assertSee('admin@uneatlantico.es');
                   
        });       
    }
     /**
     * testshowsUsersList by interface.
     *
     * @return void
     */
    /*function testshowsUsersList2()
    {     

        $this->browse(function ($browse) {
            $browse ->loginAs(User::find(1))
                    ->visit('/home')              
                    ->clickLink('@MyProfile')
                    ->clickLink('@users')
                    ->assertSee('Jose Maqrroquin')
                    ->assertSee('jose.marroquin@alumnos.uneatlantico.es')
                    ->assertSee('Juan Tortajada')
                    ->assertSee('juan.tortajada@uneatlantico.es');
              
        });       
    }*/

    /**
     * testUsersCreate by route.
     *
     * @return void
     */
    function testUsersCreate()
    {     

        $this->browse(function ($browse) {
            $browse ->loginAs(User::find(1))
                    ->visit('/users/create')
                    ->waitForText('Usuario')
                    ->assertSee('Usuario')
                    ->type('name', 'Pruebas Pruebas')
                    ->type('email','pruebas@alumnos.uneatlantico.es')
                    ->check('roles[]',1)               
                    ->press('Enviar')
                    ->visit('/users')
                    ->assertSee('Pruebas Pruebas');
                
        });       
    }
    /**
     * testshowsUsersDetails by route.
     *
     * @return void
     */
    function testshowsUsersDetails()
    {     
        $id=DB::table('users')->where('name','Pruebas Pruebas')->where('deleted_at',null)->value('id');
        
        $this->browse(function ($browse) use ($id) {
            $browse ->loginAs(User::find(1))
                    ->visit('/users/'.$id)
                    ->waitForText('Usuario')
                    ->assertSee('Usuario')
                    ->assertInputValue('name', 'Pruebas Pruebas')
                    ->assertInputValue('email','pruebas@alumnos.uneatlantico.es')
                    ->assertChecked('roles[]',1);
              
        });       
    }

    /**
     * testshowsUsersDetails by interface.
     *
     * @return void
     */
    function testshowsUsersDetails2()
    {     
        $id=DB::table('users')->where('name','Pruebas Pruebas')->where('deleted_at',null)->value('id');
        $this->browse(function ($browse) use ($id) {
            $browse ->loginAs(User::find(1))
                    ->visit('/users')
                    ->waitForText('Usuarios')
                    ->assertSee('Usuarios')
                    ->click('@show'.$id)
                    ->assertInputValue('name', 'Pruebas Pruebas')
                    ->assertInputValue('email','pruebas@alumnos.uneatlantico.es')
                    ->assertChecked('roles[]',1);
              
        });       
    }

    /**
     * testUserEdit by route.
     *
     * @return void
     */
    function testUserEdit()
    {     
        $id=DB::table('users')->where('name','Pruebas Pruebas')->where('deleted_at',null)->value('id');
       
        $this->browse(function ($browse) use ($id) {
            $browse ->loginAs(User::find(1))
                    ->visit('/users/'.$id.'/edit')
                    ->waitForText('Usuario')
                    ->assertSee('Usuario')
                    ->assertInputValue('name', 'Pruebas Pruebas')
                    ->assertInputValue('email','pruebas@alumnos.uneatlantico.es')
                    ->assertChecked('roles[]',1)
                    ->type('name','Hola')
                    ->press('Enviar')
                    ->visit('/users/'.$id.'/')
                    ->assertInputValue('name', 'Hola');
              
        });       
    }

     /**
     * testUserEdit2 by route.
     *
     * @return void
     */
    function testUserEdit2()
    {     
        $id=DB::table('users')->where('name','Hola')->where('deleted_at',null)->value('id');
        $this->browse(function ($browse) use ($id) {
            
            $browse ->loginAs(User::find(1))
                    ->visit('/users')
                    ->waitForText('Usuarios')
                    ->assertSee('Usuarios')
                    ->click('@edit'.$id)
                    ->assertInputValue('name', 'Hola')                    
                    ->assertInputValue('email','pruebas@alumnos.uneatlantico.es')
                    ->assertChecked('roles[]',1)
                    ->type('name','Pruebas Pruebas')
                    ->press('Enviar')
                    ->visit('/users/'.$id.'/')
                    ->assertInputValue('name', 'Pruebas Pruebas');
              
        });       
    }
    /**
     * testUsersDelete by interface.
     *
     * @return void
     */
    function testUsersDelete()
    {     

       /* $this->browse(function ($browse) {
            $browse ->loginAs(User::find(1))
                    ->visit('/users')
                    ->assertVisible('#dataTable > tbody:nth-child(3) > tr:nth-child(1) > td:nth-child(4) > form:nth-child(1) > div:nth-child(3) > button:nth-child(3)')
                    ->visit($browse->press('#dataTable > tbody:nth-child(3) > tr:nth-child(1) > td:nth-child(4) > form:nth-child(1) > div:nth-child(3) > button:nth-child(3)', 'href')->acceptDialog()->assertSee('Usuarios'));
                    //->visit('/users')
                                 
       }); */      
    }

    
  
    
}